package arbol;

import javax.swing.JOptionPane;

/**
 *
 * @author hitzu
 */
public class ArbolBinario {

    NodoArbol raiz;

    public ArbolBinario() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol nuevo = new NodoArbol(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol auxiliar = raiz;
            NodoArbol padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.datoin) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
      
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void inOrden(NodoArbol r) {       
        if (r != null) {
            inOrden(r.hijoizquierdo);
            JOptionPane.showMessageDialog(null, r.datoin);
            inOrden(r.hijoderecho);
        }
    }

    public void preOrden(NodoArbol r) {    
        if (r != null) {
            JOptionPane.showMessageDialog(null, r.datopre);
            preOrden(r.hijoizquierdo);
            preOrden(r.hijoderecho);
        }
    }

    public void postOrden(NodoArbol r) {
        if (r != null) {          
            postOrden(r.hijoizquierdo);
            postOrden(r.hijoderecho);
            JOptionPane.showMessageDialog(null,r.datopos);
        }
    }
}
