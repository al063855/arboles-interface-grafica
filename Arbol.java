package arbol;

import javax.swing.JOptionPane;

/**
 *
 * @author hitzu
 */
public class Arbol  {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int dato;
        String nombre;
        ArbolBinario miArbolin = new ArbolBinario();
        miArbolin.agregarNodo(25, "DEMO");
        miArbolin.agregarNodo(6, "DEMO");
        miArbolin.agregarNodo(9, "DEMO");
        miArbolin.agregarNodo(15, "DEMO");
        miArbolin.agregarNodo(3, "DEMO");
        miArbolin.agregarNodo(29, "DEMO");
        miArbolin.agregarNodo(17, "DEMO");
        miArbolin.agregarNodo(28, "DEMO");
        miArbolin.agregarNodo(10, "DEMO");
        
        ArbolBinario miArbolpre = new ArbolBinario();
        miArbolpre.agregarNodo(30, "DEMO");
        miArbolpre.agregarNodo(61, "DEMO");
        miArbolpre.agregarNodo(19, "DEMO");
        miArbolpre.agregarNodo(20, "DEMO");
        miArbolpre.agregarNodo(13, "DEMO");
        miArbolpre.agregarNodo(21, "DEMO");
        miArbolpre.agregarNodo(33, "DEMO");
        miArbolpre.agregarNodo(18, "DEMO");
        miArbolpre.agregarNodo(43, "DEMO");
        
        ArbolBinario miArbolpos = new ArbolBinario();
        miArbolpos.agregarNodo(23, "DEMO");
        miArbolpos.agregarNodo(11, "DEMO");
        miArbolpos.agregarNodo(6, "DEMO");
        miArbolpos.agregarNodo(40, "DEMO");
        miArbolpos.agregarNodo(20, "DEMO");
        miArbolpos.agregarNodo(22, "DEMO");
        miArbolpos.agregarNodo(38, "DEMO");
        miArbolpos.agregarNodo(5, "DEMO");
        miArbolpos.agregarNodo(14, "DEMO");
        
       JOptionPane.showMessageDialog(null, "InOrden");
        if (!miArbolin.estaVacio()){
            miArbolin.inOrden(miArbolin.raiz);
        }
        JOptionPane.showMessageDialog(null, "PreOrden");
        if (!miArbolpre.estaVacio()){
            miArbolpre.preOrden(miArbolpre.raiz);
        }
        JOptionPane.showMessageDialog(null, "PostOrden");
        if (!miArbolpos.estaVacio()){
            miArbolpos.postOrden(miArbolpos.raiz);
        }
    }
    
}
