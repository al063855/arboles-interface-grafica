package arbol;

/**
 *
 * @author hitzu
 */
public class NodoArbol {
    int datoin;
    int datopre;
    int datopos;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol (int d, String s){
        this.datoin = d;
        this.datopre = d;
        this.datopos = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
}

